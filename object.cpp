#include "object.h"

Object::Object(const char* name, const char* desc, bool hidden, bool gettable):
	m_name{name},
	m_desc{desc},
	m_hidden{hidden},
	m_gettable{gettable}
{};

void Object::name(const char* new_name) {
	m_name = new_name;
}

void Object::desc(const char* new_desc) {
	m_desc = new_desc;
}

void Object::addShortname(const char* new_shortname) {

	m_shortnames.push_back(new_shortname);

}

bool Object::isShortname(const char* shortname) {

	for (const char* sn : m_shortnames) {
		if (strcmp(sn, shortname) == 0) {
			return true;
		}
	}

	return false;

}