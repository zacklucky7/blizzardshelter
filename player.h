#ifndef PLAYER_H
#define PLAYER_H

#include <string>

#include "object.h"
#include "room.h"

class Player {

	//Inventory* m_inv;

	Room* m_current_room{ nullptr };
	std::string m_desc {"You are bundled up in ragged winter wear. Beneath the scraps of scavenged clothing\nis a person that has seen and had to do a lot of things to survive in this new world."};

	std::vector<Object*> m_inventory{};

	bool m_hasShovel{ false };
	bool m_hasGun{ false };

public:

	void room(Room* new_room);
	Room* room();

	std::string& desc() { return m_desc; }
	std::vector<Object*>& inventory() { return m_inventory; }

	void check_inventory();
	void drop_item(std::string& to_drop);
	void look_at(std::string& input);
	void look_around();
	int kill(std::string& input);
	void pick_up(std::string& input, bool silent = false);
	void take_room_exit(std::string& input);
	bool dig_grave(std::string& input);
	void read(std::string& input);

	void shovel(bool state) { m_hasShovel = state; }
	bool hasShovel() { return m_hasShovel; }

	void gun(bool state) { m_hasGun = state; }
	bool hasGun() { return m_hasGun; }

	void direct_add(Object*);
	Object* searchForObject(std::string& shortname);
	Object* searchInvForObject(std::string& shortname);

};

#endif