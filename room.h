#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <vector>

#include "object.h"

class Room {

	std::string m_name;
	std::string m_desc;
	std::vector<Room*> m_exits{};
	std::vector<Object*> m_objects;

public:

	const std::string& name();
	const std::string& desc();
	const std::vector<Room*>& exits();
	const std::vector<Object*>& items();

	Room(std::string name, std::string desc);
	Room(std::string name);

	void setDesc(std::string desc);

	Room* getExit(std::string& input);
	Object* searchForShortname(const std::string& shortname);
	Object* isObjectPresent(Object* item);
	void addObject(Object* item);
	void removeObject(Object* item);

	bool connectExit(Room*);

	void printRoom();

};

#endif