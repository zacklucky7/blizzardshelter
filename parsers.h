#ifndef PARSERS_H
#define PARSERS_H

#include <string>

#include "object.h"
#include "player.h"

extern Player* player;

namespace Parsers {

	const std::vector<std::string> INVENTORY_TERMS{
		"inventory",
		"inv",
		"i"
	};

	const std::vector<std::string> LOOK_AROUND_TERMS{
		"l",
		"look",
		"look around",
		"examine room",
		"examine surroundings"
	};

	const std::vector<std::string> LOOK_AT_TERMS{
		"examine ",
		"l ",
		"look ",
		"look at ",
		"look for ",
		"look at the "
	};

	const std::vector<std::string> PICK_UP_TERMS{
		"get ",
		"get the ",
		"pick up ",
		"pick up the ",
		"grab ",
		"grab the ",
		"take ",
		"take the "
	};

	const std::vector<std::string> DROP_TERMS{
		"drop ",
		"drop the ",
		"put down ",
		"put down the "
	};

	const std::vector<std::string> GO_TO_TERMS{
		"enter ",
		"enter the ",
		"go ",
		"go to ",
		"move to ",
		"walk to "
		"go to the ",
		"move to the ",
		"walk to the "
	};

	const std::vector<std::string> DIG_TERMS{
		"dig ",
		"dig for ",
		"dig up ",
		"dig up the ",
		"exhume "
	};

	const std::vector<std::string> KILL_TERMS{
		"use revolver on ",
		"attack ",
		"attack the ",
		"kill ",
		"kill the ",
		"shoot ",
		"shoot the "
	};

	const std::vector<std::string> READ_TERMS{
		"read ",
		"read the "
	};

	enum class Parse {
		LOOK_AROUND,
		LOOK_AT,
		PICK_UP,
		INVENTORY,
		DROP_ITEM,
		GO_TO,
		DIG_UP,
		KILL,
		READ,
		UNKNOWN,
		MAX_TYPES
	};

	Parse isRecognized(std::string& input);
	std::string splitInput(Parse pType, std::string& input);

	bool isExactTerm(const std::vector<std::string>& terms, const std::string& input);
	bool inTerms(const std::vector<std::string>& terms, const std::string& input);

	bool isDig(std::string& input);
	bool isDropped(std::string& input);
	bool isGoTo(std::string& input);
	bool isKill(std::string& input);
	bool isInventory(std::string& input);
	bool isLookAround(std::string& input);
	bool isLookAt(std::string& input);
	bool isPickUp(std::string& input);
	bool isRead(std::string& input);

}

#endif