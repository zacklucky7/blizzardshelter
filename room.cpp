#include "room.h"

Room::Room(std::string name) :
	m_name{name},
	m_desc{ "no desc" }
{};

Room::Room(std::string name, std::string desc):
	m_name{name},
	m_desc{desc}
{};

const std::string& Room::name() {
	return m_name;
}

const std::string& Room::desc() {
	return m_desc;
}

const std::vector<Room*>& Room::exits() {
	return m_exits;
}

void Room::setDesc(std::string desc) {
	m_desc = desc;
}

bool Room::connectExit(Room* toConnect) {

	// TODO: Make code that checks if the exit is already present.

	m_exits.push_back(toConnect);
	return true;

}

Object* Room::searchForShortname(const std::string& shortname) {

	std::string new_shortname = shortname;
	const char* shortname_c = new_shortname.data();

	if (m_objects.size() > 0) {
		for (auto obj : m_objects) {
			if (obj->isShortname(shortname_c)) {
				return obj;
			}
		}
	}

	return nullptr;

}

Room* Room::getExit(std::string& input) {

	Room* exit = nullptr;

	for (auto room: exits()) {
		if (room->name().find(input) != std::string::npos) {
			exit = room;
			break;
		}
	}
	
	return exit;

}

Object* Room::isObjectPresent(Object* item) {

	if (m_objects.size() > 0) {
		for (int i = 0; i < m_objects.size(); i++) {
			if (m_objects.at(i) == item) {
				return m_objects.at(i);
			}
		}
	}

	return nullptr;

}

void Room::addObject(Object* item) {

	// Check if the item is there already.

	if (isObjectPresent(item) == nullptr) {
		m_objects.push_back(item);
	}

}

void Room::removeObject(Object* item) {

	Object* obj = isObjectPresent(item);

	for (int i = 0; i < m_objects.size(); i++) {
		if (m_objects.at(i) == obj) {
			m_objects.erase(m_objects.begin()+i);
			break;
		}
	}

}

void Room::printRoom() {

	//printf("[%s]\n\n", name().c_str());
	printf("%s\n\n", desc().c_str());

	if (m_objects.size() > 0) {
		for (auto obj : m_objects) {
			if (!obj->hidden()) {
				printf("The %s is here.\n", obj->name().data());
			}
		}
		printf("\n");
	}

	if (exits().size() > 0) {
		for (int i = 0; i < exits().size(); i++) {
			auto exit = exits().at(i);
			printf("There is a way to %s here.\n", exit->name().c_str());
		}
	}
	else {
		puts("There are no exits from this room.");
	}

}