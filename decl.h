#ifndef DECL_H
#define DECL_H

enum class Ending {
	SUCCUMB,
	SUICIDE,
	BEARFOOD,
	SAFETY,
	UNKNOWN
};

Ending ending = Ending::SUCCUMB;

void intro();
void printTitle();
void tellStory();
void giveInstructions();

bool setupGame();
void cleanupGame();
void endGame();

void playGame();

bool parseCommand(std::string& input);
void toLower(std::string* input);

#endif