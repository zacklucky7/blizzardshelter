#include "parsers.h"

#include <iostream>
#include <string>
#include <vector>

bool Parsers::inTerms(const std::vector<std::string>& terms, const std::string& input) {

	for (auto term : terms) {
		auto found = input.find(term);
		if (found != std::string::npos) {
			return true;
		}
	}

	return false;

}

bool Parsers::isExactTerm(const std::vector<std::string>& terms, const std::string& input) {

	for (auto term : terms) {
		if (input == term) {
			return true;
		}
	}

	return false;

}

Parsers::Parse Parsers::isRecognized(std::string& input) {

	if (isLookAround(input)) {
		return Parse::LOOK_AROUND;
	}

	if (isInventory(input)) {
		return Parse::INVENTORY;
	}

	if (isLookAt(input)) {
		return Parse::LOOK_AT;
	}

	if (isPickUp(input)) {
		return Parse::PICK_UP;
	}

	if (isDropped(input)) {
		return Parse::DROP_ITEM;
	}

	if (isGoTo(input)) {
		return Parse::GO_TO;
	}

	if (isDig(input)) {
		return Parse::DIG_UP;
	}

	if (isKill(input)) {
		return Parse::KILL;
	}

	if (isRead(input)) {
		return Parse::READ;
	}

	return Parse::UNKNOWN;

}

std::string Parsers::splitInput(Parse pType, std::string& input) {

	const std::vector<std::string>* sterms = nullptr;

	std::string to_find{""};

	switch (pType) {
		case Parse::LOOK_AT: {
			sterms = &LOOK_AT_TERMS;
			break;
		}
		case Parse::PICK_UP: {
			sterms = &PICK_UP_TERMS;
			break;
		}
		case Parse::DROP_ITEM: {
			sterms = &DROP_TERMS;
			break;
		}
		case Parse::GO_TO: {
			sterms = &GO_TO_TERMS;
			break;
		} case Parse::DIG_UP: {
			sterms = &DIG_TERMS;
			break;
		} case Parse::KILL: {
			sterms = &KILL_TERMS;
			break;
		} case Parse::READ: {
			sterms = &READ_TERMS;
			break;
		}
		default: {
			printf("ERROR: Unknown case passed to splitInput\n");
		}
	}

	if (sterms) {

		// Now that we have the terms to use, split out what we're actually looking for.

		for (auto term : *sterms) {
			size_t term_length = term.size();
			if (input.substr(0, term_length) == term) {
				to_find = input.substr(term_length);
			}
		}

	}

	return to_find;

}

bool Parsers::isDropped(std::string& input) {

	if (inTerms(DROP_TERMS, input)) {
		return true;
	}

	return false;

}

bool Parsers::isDig(std::string& input) {

	if (inTerms(DIG_TERMS, input)) {
		return true;
	}

	return false;
}

bool Parsers::isGoTo(std::string& input) {

	if (inTerms(GO_TO_TERMS, input)) {
		return true;
	}

	return false;

}

bool Parsers::isInventory(std::string& input) {

	if (isExactTerm(INVENTORY_TERMS, input)) {
		return true;
	}

	return false;

}

bool Parsers::isKill(std::string& input) {

	if (inTerms(KILL_TERMS, input)) {
		return true;
	}

	return false;

}

bool Parsers::isLookAround(std::string& input) {

	if (isExactTerm(LOOK_AROUND_TERMS, input)) {
		return true;
	}

	return false;

}

bool Parsers::isLookAt(std::string& input) {

	if (inTerms(LOOK_AT_TERMS, input)) {
		return true;
	}

	return false;

}

bool Parsers::isPickUp(std::string& input) {

	if (inTerms(PICK_UP_TERMS, input)) {
		return true;
	}

	return false;

}

bool Parsers::isRead(std::string& input) {

	if (inTerms(READ_TERMS, input)) {
		return true;
	}

	return false;

}