#ifndef OBJECT_H
#define OBJECT_H

#include <iostream>
#include <string>
#include <vector>

class Object {

	std::vector<const char*> m_shortnames;
	std::string m_name;
	std::string m_desc;
	bool m_hidden;
	bool m_gettable;

public:

	Object(const char* name, const char* desc, bool hidden = false, bool gettable = true);

	bool hidden() { return m_hidden; }
	void hidden(bool state) { m_hidden = state; }

	bool gettable() { return m_gettable; }

	void name(const char* new_name);
	std::string& name() { return m_name; }

	void desc(const char* new_desc);
	std::string& desc() { return m_desc; }

	void addShortname(const char* new_shortname);
	bool isShortname(const char* shortname);

};

#endif