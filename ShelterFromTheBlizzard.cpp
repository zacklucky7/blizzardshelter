#include <cstring>
#include <locale>
#include <iostream>
#include <sstream>
#include <vector>

#include "decl.h"
#include "parsers.h"

#include "object.h"
#include "player.h"
#include "room.h"

std::vector<Room*> all_rooms{};
Room* grave_tree{ nullptr };    // Quick reference to an important pointer, but should be in its own section
std::vector<Object*> all_objects{};
Player* player{ nullptr };

int main()
{
    intro();

    if (setupGame()) {
        playGame();
    }

    cleanupGame();

    return 0;
}

void intro() {

    printTitle();
    tellStory();
    giveInstructions();

}

void playGame() {

    player->look_around();

    bool playing{ true };

    std::string input{};

    while (playing) {
        std::cout << "\n---\nEnter a command: ";
        getline(std::cin, input);
        std::cout << "---\n";

        playing = parseCommand(input);

    }

    endGame();

}

void endGame() {

    switch (ending) {
        case Ending::SUCCUMB: {
            printf("You give in to the raging blizzard.\nYour remains are never found.\n\nENDING 1 (APATHY)\n");
            break;
        } case Ending::SUICIDE: {
            printf("\nThis new world is too much for you to bear.\nYou've had enough.\n\nENDING 2 (SURRENDER)\n");
            break;
        } case Ending::BEARFOOD: {
            printf("\nYou fought valiantly, but you were no match for an angry bear.\nAt least it got something to eat.\n\nENDING 3 (GLORY)\n");
            break;
        } case Ending::SAFETY: {
            printf("You manage to kill the bear and take shelter in the bunker.\n"
                "You have plenty of food and water to last you for a long time.\n"
                "Who knows what will happen next in this new, dangerous world?\n\nENDING 4 (SAFETY)\n");
            break;
        } default: {
            printf("\nSomething strange has happened. You've received an ending\n"
                "that shouldn't be possible. Congratulations!\n\nENDING 5 (OOPS)");
            break;
        }
    }

    std::string dummy{};
    std::cin >> dummy;

}

void addRoom(Room* room) {

    all_rooms.push_back(room);

}

void addObj(Object* obj) {

    all_objects.push_back(obj);
}

void buildRooms();

bool setupGame() {

    buildRooms();

    return true;

}

void buildRooms() {

    // The starting room

    Room* start_room = new Room("a snowy field outside a bunker", "You are in a snowy field outside of an abandoned bunker.\nThe blizzard is picking "
        "up in ferocity, and you must find shelter soon.");

    Object* sr_bunker_view = new Object("abandoned bunker", "You can see an abandoned bunker up ahead. The vault door is ajar, and some snow\n"
        "sweeps into the entryway.", true, false);
    sr_bunker_view->addShortname("bunker");
    sr_bunker_view->addShortname("shelter");

    Object* sr_storm_view = new Object("blizzard", "The blizzard hasn't let up for days. It's a miracle you've gotten this far.", true, false);
    sr_storm_view->addShortname("blizzard");
    sr_storm_view->addShortname("weather");
    sr_storm_view->addShortname("storm");
    sr_storm_view->addShortname("snowstorm");

    start_room->addObject(sr_bunker_view);
    start_room->addObject(sr_storm_view);

    addObj(sr_bunker_view);
    addObj(sr_storm_view);
    addRoom(start_room);

    // The grave

    grave_tree = new Room("a lone tree", "A lonely, dead tree stands here amidst a field of endless snow.");

    Object* gt_tree_view = new Object("dead tree", "A lone birch tree stands here, long-dead.\nIts branches reach into the sky like "
        "the fingers of a dying man.", true, false);
    gt_tree_view->addShortname("tree");
    gt_tree_view->addShortname("dead tree");
    gt_tree_view->addShortname("birch");

    Object* gt_grave_view = new Object("hidden grave", "Almost imperceptible, a slightly raised mound of snow indicates the grave\n"
        "mentioned in the old journal.", true, false);
    gt_grave_view->addShortname("grave");
    gt_grave_view->addShortname("hidden grave");
    gt_grave_view->addShortname("harry"); // lol

    start_room->connectExit(grave_tree);
    grave_tree->connectExit(start_room);

    grave_tree->addObject(gt_tree_view);
    grave_tree->addObject(gt_grave_view);

    addObj(gt_tree_view);
    addObj(gt_grave_view);
    addRoom(grave_tree);

    // The entryway

    Room* entryway = new Room("the entryway of the bunker", "The bunker, mercifully, shelters you from the buffeting winds of the blizzard\n"
        "outside. It's still very cold here.");

    Object* ew_spade = new Object("old spade", "This old spade looks brittle from age, but might still be useful.");
    ew_spade->addShortname("old spade");
    ew_spade->addShortname("spade");
    ew_spade->addShortname("old shovel");
    ew_spade->addShortname("shovel");
    ew_spade->addShortname("tool");

    entryway->connectExit(start_room);
    start_room->connectExit(entryway);

    entryway->addObject(ew_spade);

    addObj(ew_spade);
    addRoom(entryway);

    // The living area

    Room* living_area = new Room("the living area", "The air is heavy with the smell of an animal that spends a lot of its time here.\n"
        "The furniture is torn apart or knocked over. The far exit is blocked by\n"
        "the form of a large, sleeping bear.");

    Object* la_bear = new Object("sleeping bear", "A large bear is asleep here. It looks deadly, like it could tear you apart in\nthe blink of an eye if it wanted to. Tread carefully.", false, false);
    la_bear->addShortname("bear");
    la_bear->addShortname("sleeping bear");
    la_bear->addShortname("animal");

    living_area->connectExit(entryway);
    entryway->connectExit(living_area);

    living_area->addObject(la_bear);

    addObj(la_bear);
    addRoom(living_area);

    // The storage room

    Room* storage_room = new Room("the storage room", "You're overjoyed to find a stocked storage room.\n"
        "While some of the supplies are missing, the vast majority of them are untouched.\n"
        "You could wait out the blizzard if you can secure this bunker.");
    
    Object* sr_journal = new Object("old journal", "An old journal. A lot of the writing has faded with time.\nThe author's name is listed as \"Laura L.\"");
    sr_journal->addShortname("journal");
    sr_journal->addShortname("old journal");

    storage_room->addObject(sr_journal);

    storage_room->connectExit(entryway);
    entryway->connectExit(storage_room);
    storage_room->connectExit(living_area);
    living_area->connectExit(storage_room);

    player = new Player();
    player->room(start_room);

    Object* canteen = new Object("empty canteen", "Your canteen of water. It's empty...");
    canteen->addShortname("canteen");
    player->direct_add(canteen);

    addObj(canteen);

}

void cleanupGame() {

    for (auto ptr : all_rooms) {
        delete ptr;
    }

    delete player;
    player = nullptr;

    for (auto object : all_objects) {
        delete object;
        object = nullptr;
    }

}

bool parseCommand(std::string& input) {

    // Returns false if the game is over.

    toLower(&input);

    std::cout << '\n';

    if (input == "quit" || input == "q") {
        ending = Ending::SUCCUMB;
        return false;
    }
    else if (input == "help") {
        giveInstructions();
        return true;
    }
    else {
        Parsers::Parse parse = Parsers::isRecognized(input);

        switch (parse) {
        case Parsers::Parse::LOOK_AROUND: {
            player->look_around();
            break;
        }
        case Parsers::Parse::INVENTORY: {
            player->check_inventory();
            break;
        }
        case Parsers::Parse::LOOK_AT: {
            input = Parsers::splitInput(Parsers::Parse::LOOK_AT, input);
            player->look_at(input);
            break;
        }
        case Parsers::Parse::PICK_UP: {
            input = Parsers::splitInput(Parsers::Parse::PICK_UP, input);
            player->pick_up(input);
            break;
        }
        case Parsers::Parse::DROP_ITEM: {
            input = Parsers::splitInput(Parsers::Parse::DROP_ITEM, input);
            player->drop_item(input);
            break;
        } case Parsers::Parse::GO_TO: {
            input = Parsers::splitInput(Parsers::Parse::GO_TO, input);
            player->take_room_exit(input);
            break;
        } case Parsers::Parse::DIG_UP: {
            input = Parsers::splitInput(Parsers::Parse::DIG_UP, input);
            if (player->dig_grave(input)) {
                Object* revolver = new Object("loaded revolver", "A cold, old revolver. It's fully loaded with six shots.");
                revolver->addShortname("revolver");
                revolver->addShortname("gun");
                revolver->addShortname("pistol");
                revolver->addShortname("weapon");
                addObj(revolver);

                for (auto room : all_rooms) {
                    if (strcmp(room->name().data(), "a lone tree") == 0) {
                        room->addObject(revolver);
                        break;
                    }
                }
            };
            break;
        } case Parsers::Parse::KILL: {
            input = Parsers::splitInput(Parsers::Parse::KILL, input);
            int ending_c = player->kill(input);
            // -1 means continue. 0 is SURRENDER. 1 is GLORY. 2 is SAFETY.
            if (ending_c > -1) {
                if (ending_c == 0) {
                    ending = Ending::SUICIDE;
                    return false;
                }
                else if (ending_c == 1) {
                    ending = Ending::BEARFOOD;
                    return false;
                }
                else if (ending_c == 2) {
                    ending = Ending::SAFETY;
                    return false;
                }
                else {
                    ending = Ending::UNKNOWN;
                    return false;
                }
            }
            break;
        } case Parsers::Parse::READ: {
            input = Parsers::splitInput(Parsers::Parse::READ, input);
            player->read(input);
            if (input == "journal" || input == "old journal") {
                for (auto obj : all_objects) {
                    if (strcmp(obj->name().data(), "hidden grave") == 0) {
                        obj->hidden(false);
                        break;
                    }
                }
            }
            break;
        }
        case Parsers::Parse::UNKNOWN:
        default: {
            std::cout << "Unknown command." << std::endl;
        }
        }

        return true;

    }
}

void toLower(std::string* input) {
    
    std::locale loc;
    for (std::string::size_type i = 0; i < (*input).length(); ++i) {
        (*input)[i] = std::tolower((*input)[i], loc);
    }

}

void printTitle() {


    puts("\n-----------------------------");
    puts("* SHELTER FROM THE BLIZZARD *");
    puts("-----------------------------\n");

}

void tellStory() {

    printf("You've been walking for days. You're out of food and on the last few drops of your water.\n"
        "The blizzard has come again. You won't survive if you don't find shelter.\n"
        "\nWait -- what's that? Is that a bunker up ahead?\n");

}

void giveInstructions() {

    printf("\n-----------------------------\n\n");
    printf("This game is a text adventure. To do an action, you will need to type out what you want to do.\nFor example, you can do LOOK AROUND, or PICK UP AXE, or GO TO THE BUNKER. \n\nTo see this message, type HELP. To check your inventory, use I, INV, or INVENTORY.\n");
    printf("\n-----------------------------\n\n");

}