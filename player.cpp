#include "player.h"

void Player::room(Room* new_room) {
	m_current_room = new_room;
}

Room* Player::room() {
	return m_current_room;
}

bool Player::dig_grave(std::string& input) {

	if (hasShovel()) {
		
		auto grave = searchForObject(input);

		if (grave) {
			if (grave->isShortname("grave") && strcmp(grave->name().data(), "hidden grave") == 0) {
				printf("You set to the grim task of unearthing the grave. Eventually, you\n"
					"dig up an old skeleton. Old scraps of cloth cling to its bones, but\n"
					"most notably, a revolver has been placed on its chest.\n");
				grave->name("unearthed grave");
				grave->desc("This grave has been unearthed. Someone's skeleton has\n"
					"been revealed, and snow is beginning to pile on it.");
				grave->hidden(false);
				return true;
			}
		}

		printf("There's no %s here.\n", input.data());

	}
	else {
		printf("You have nothing to dig with.\n");
	}

	return false;

}

int Player::kill(std::string& input) {

	if (input == "me" || input == "myself" || input == "self" || input == "yourself") {
		if (hasGun()) {
			while (true) {
				printf("Are you sure? (yes or no) ");
				std::string choice;
				getline(std::cin, choice);

				if (choice == "yes") {
					return 0;
				} else if (choice == "no") {
					return -1;
				}
			}
		}
		else {
			printf("With what, your bare hands?\n");
			return -1;
		}
	}
	else {

		auto bear = searchForObject(input);

		if (bear) {
			if (strcmp(bear->name().data(), "sleeping bear") == 0) {
				if (hasGun()) {
					return 2;	// SAFETY ENDING
				}
				else {
					while (true) {
						printf("With your bare hands? (yes or no) ");
						std::string choice;
						getline(std::cin, choice);

						if (choice == "yes") {
							return 1;	// BEAR FOOD ENDING
						}
						else if (choice == "no") {
							return -1;	// continue the game
						}
					}
				}
			}
			else {
				printf("You can't kill the %s.\n", input.data());
				return -1;
			}
		}
		else {
			printf("You don't see the %s to kill.\n", input.data());
			return -1;	// nothing shot nothing gained
		}
	}

}

void Player::look_at(std::string& input) {

	if (input == "me" || input == "myself" || input == "self") {
		printf("%s\n", desc().data());
		return;
	}

	Object* item = searchInvForObject(input);
	
	if (!item) {
		item = searchForObject(input);
	}

	if (item) {
		//printf("You see a %s here.\n", item->name().data());
		printf("%s\n", item->desc().data());
	}
	else {
		printf("You don't see the %s here.\n", input.data());
	}

}

void Player::take_room_exit(std::string& input) {

	Room* exit = room()->getExit(input);

	if (exit) {
		room(exit);
		look_around();
	}
	else {
		printf("You don't see that exit.\n");
	}

}

void Player::check_inventory() {

	if (inventory().size() > 0) {
		printf("You're carrying...");
		for (auto item : inventory()) {
			printf("\n...the %s.", item->name().data());
		}
		printf("\n");
	}
	else {
		printf("Your inventory is empty.\n");
	}

}

void Player::direct_add(Object* item) {

	inventory().push_back(item);

}

void Player::drop_item(std::string& to_drop) {

	auto droppable = searchInvForObject(to_drop);

	if (droppable) {
		for (auto i = 0; i < inventory().size(); i++) {
			if (inventory().at(i) == droppable) {
				inventory().erase(inventory().begin()+i);
				room()->addObject(droppable);
				printf("You drop the %s.\n", droppable->name().data());
				if (strcmp(droppable->name().data(), "old spade") == 0) {
					shovel(false);
				}
				if (strcmp(droppable->name().data(), "loaded revolver") == 0) {
					gun(false);
				}
			}
		}
	}
	else {
		printf("You don't have the %s.\n", to_drop.data());
	}

}

void Player::pick_up(std::string& input, bool silent) {

	auto item = searchForObject(input);

	if (item) {

		bool has = false;

		if (inventory().size() > 0) {
			for (auto inv_item : inventory()) {
				if (item == inv_item) {
					if (!silent) {
						printf("You already have the %s.\n", item->name().data());
					}
					has = true;
					break;
				}
			}
		}

		if (!has && item->gettable()) {
			inventory().push_back(item);
			if (!silent) {
				printf("You pick up the %s.\n", item->name().data());
				if (strcmp(item->name().data(), "old spade") == 0) {
					shovel(true);
				}
				if (strcmp(item->name().data(), "loaded revolver") == 0) {
					gun(true);
				}
			}
			room()->removeObject(item);
		}
		else if (!has && !item->gettable()) {
			if (!silent) {
				printf("You can't pick up the %s.\n", item->name().data());
			}
		}

	}
	else {
		if (!silent) {
			printf("You don't see the %s to pick up.\n", input.data());
		}
	}

}

void Player::look_around() {
	
	if (room() != nullptr) {
		room()->printRoom();
	}

}

void Player::read(std::string& input) {

	Object* item = searchInvForObject(input);

	if (!item) {
		item = searchForObject(input);
	}

	if (item) {
		if (strcmp(item->name().data(), "old journal") == 0) {
			printf("Much of the text is too faded to read, but you find something.\n\n"
				"\"Buried Harry today. The sickness was too much for him to handle.\n"
				"Poor guy. He didn't deserve to go out like that. I'm taking some\n"
				"of the food from this bunker and I'm going to head north to the\n"
				"rendezvous point. Wish I could stay here for a little longer.\n"
				"Put him in the grave with his \"magic wand\" he was so proud of,\n"
				"at the foot of that lonely birch outside. I'll head out soon.\"\n");
		}
		else {
			printf("There's nothing to read on the %s.\n", item->name().data());
		}
		
	}
	else {
		printf("You don't see the %s here.\n", input.data());
	}

}

Object* Player::searchInvForObject(std::string& shortname) {

	if (inventory().size() > 0) {
		for (auto obj : inventory()) {
			if (obj->isShortname(shortname.data())) {
				return obj;
			}
		}
	}

	return nullptr;

}



Object* Player::searchForObject(std::string& shortname) {
	
	Object* obj = room()->searchForShortname(shortname);

	if (obj) {
		return obj;
	}

	return nullptr;

}